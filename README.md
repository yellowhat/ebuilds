# Yellowhat's ebuilds repository

## Add this repository

```bash
git clone https://gitlab.com/yellowhat-labs/ebuilds

sudo bash -c "cat >/etc/portage/repos.conf/larry.conf" <<EOL
[larry]
location = $PWD/ebuilds
EOL
```

## Package list

* `dev-python/i3ipc`
* `dev-python/pywayland`
* `dev-python/pywlroots`
* `dev-python/sf`
* `dev-python/xkbcommon`
* `gui-apps/gammastep`
* `gui-apps/statusbar`
* `gui-apps/warpd`
* `gui-apps/wev`
* `gui-apps/wlsunset`
* `media-fonts/comic-mono`
* `media-fonts/joypixels`
* `x11-terms/kitty`
* `x11-terms/wayst`
* `x11-wm/qtile`
