# Copyright 2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="A modal keyboard-driven virtual pointer"
HOMEPAGE="https://github.com/rvaiya/warpd"

if [[ ${PV} == 9999 ]]; then
    EGIT_REPO_URI="https://github.com/rvaiya/warpd"
	inherit git-r3
else
	SRC_URI="https://github.com/rvaiya/${PN}/archive/refs/tags/v${PV}.zip"
fi

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

DEPEND="
	dev-libs/wayland
	x11-libs/libxkbcommon"
RDEPEND="${DEPEND}"
BDEPEND="
	dev-libs/wayland
    x11-libs/cairo
"

src_compile() {
    DISABLE_X=1 emake
}

src_install() {
    dobin bin/warpd
}
